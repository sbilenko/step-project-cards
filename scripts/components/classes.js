export class Modal {
    constructor(idModal,label,title, body) {
        this.idModal = idModal;
        this.label = label;
        this.title = title;
        this.body = body;
    }

    render () {
        const modalParent = document.querySelector('.root');
                document.body.insertAdjacentHTML('beforeend',
                    `<div class="modal-backdrop fade show"></div>`);
                modalParent.insertAdjacentHTML('afterend', `
        <div class="modal fade show" id="${this.idModal}" tabindex="-1"
            aria-labelledby="${this.label}" aria-hidden="true" style="display:block">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title fs-6" id="${this.label}">${this.title}</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        ${this.body}
                    </div>
                </div>
            </div>
        </div>
        `);
    }


};


export class ModalLogin extends Modal {
    constructor (idModal = 'authorization-form-modal',label = 'authorizationModalLabel',title = 'Авторизація', email = '', password = '') {
    super( idModal, label, title )
    this.email = email;
    this.password = password;
    this.body = `
    <form>
        <div class="mb-3 mt-3">
            <label for="email" class="form-label">Email:</label>
            <input type="email" class="form-control" id="email" value="${this.email}" required
                placeholder="Введіть адресу електронної пошти" name="email">
        </div>
        <div class="mb-3">
            <label for="pwd" class="form-label">Пароль</label>
            <input type="password" class="form-control" id="pwd" value="${this.password}"  required 
                placeholder="Введіть пароль" name="pswd">
        </div>
        <div class="wrapper__invalid-message">
            <p class="invalid-message text-center text-danger mt-2 mb-0"></p>
        </div>
        <button type="submit" class="btn btn-send">Надіслати</button>
    </form>
    `;
    }

    emptyInputMessage() {
        const invalidMessage = document.querySelector('.invalid-message')
        invalidMessage.innerHTML = 'Ви ввели не всі дані!';
    }
    removeInvalidMessage() {
        const invalidMessage = document.querySelector('.invalid-message')
        invalidMessage.innerHTML = '';
    }

}


export class ModalSelectDoctor extends Modal {
    constructor (idModal = 'select-doctor-modal',label = 'selectDoctorModalLabel',title = 'Створити візит',  purposeVisit = '', description = '', fullName = '', urgency = '', systolicPressure = '', diastolicPressure = '', age = '', bmi = '', cardiovascularDiseases = '', lastVisitDate = '' ) {
    super( idModal, label, title )
    this.purposeVisit = purposeVisit;
    this.description = description;
    this.fullName = fullName;
    this.urgency = urgency;
    this.systolicPressure = systolicPressure;
    this.diastolicPressure = diastolicPressure;
    this.lastVisitDate = lastVisitDate;
    this.age = age;
    this.bmi = bmi;
    this.cardiovascularDiseases = cardiovascularDiseases;
    this.body = `
    <form class="doctorVisitForm">
        <div class="mb-3">
            <select id="select-doctor" class="form-select form-select-sm" required aria-label="${this.label}">
                <option value="all" disabled selected>Виберіть лікаря</option>
                <option value="cardiologist">Кардіолог</option>
                <option value="dentist">Стоматолог</option>
                <option value="therapist">Терапевт</option>
            </select>
        </div>
        <div id="all-doctors-fields" class="disappear">
            <div class="row mb-1 d-flex justify-content-between">
                <label for="#purpose-visit" class="col-sm-4 col-form-label">Мета візиту:</label>
                <div class="col-sm-8">
                    <input type="text" id="purpose-visit" class="form-control form-control-sm mb-1" required value="${this.purposeVisit}">
                </div>
            </div>
            <div class="mb-1 d-flex justify-content-between">
                <label for="#description" class="col-sm-4 col-form-label">Короткий опис візиту:</label>
                <div class="col-sm-8">
                    <textarea id="description" class="form-control mb-1" required rows="2" value="${this.description}"></textarea>
                </div>       
            </div>
            <div class="row mb-1 d-flex justify-content-between">
                <label for="#select-urgency" class="col-sm-4 col-form-label">Терміновість:</label>
                <div class="col-sm-8">
                    <select id="select-urgency" class="form-select form-select-sm" required aria-label="Терміновість<">
                        <option value="all-urgency" disabled selected></option>
                        <option value="normal" class=" text-warning">звичайна</option>
                        <option value="priority">пріоритетна</option>
                        <option value="urgent">невідкладна</option>
                    </select>
                </div>
            </div>
            <div class="row mb-2 d-flex justify-content-between">
                <label for="#full-name" class="col-sm-2 col-form-label">ПІБ:</label>
                <div class="col-sm-10">
                    <input type="text" id="full-name" class="form-control form-control-sm mb-2" required value="${this.fullName}">
                </div>
            </div>
        </div>
        <div id="cardiologist-fields" class="row disappear">
            <div class="row mb-2 d-flex justify-content-between">
                <label for="#usual-pressure" class="col-6 col-form-label">Ваш звичайний тиск:</label>
                <div class="input-group mb-2">
                    <input type="number" id="systolic" placeholder="cистолічний" name="systolicPressure" class="form-control col-6" min="80" max="160" value="${this.systolicPressure}">
                    <input type="number" id="diastolic" placeholder="діастолічний" name="diastolicPressure" class="form-control col-6" min="60" max="100" value="${this.diastolicPressure}">
                </div>
            </div>
            <div class="row col-md-6 col-sm-12 mb-2">
                <label for="#age-cardio" class="col-sm-3 col-form-label">Вік:</label>
                <div class="col-sm-9">
                    <input type="number" id="age-cardio" class="form-control mb-2" name="age" min="1" max="130" value="${this.age}">
                </div>
            </div>
            <div class=" row col-md-6 col-sm-12  mb-2">
                <label for="#bmi" class="col-sm-3 col-form-label">ІМТ:</label>
                <div class="col-sm-9">
                    <input type="number" id="bmi" class="form-control mb-2" name="bodyMassIndex" min="18" max="50" value="${this.bmi}">
                </div>
            </div>
            <div class="mb-2 col-sm-12">
                <label for="#cardiovascular-diseases" class="col-sm-12 col-form-label">Перенесені захворювання серцево-судинної системи:</label>
                <div class="col-sm-12">
                    <textarea id="cardiovascular-diseases" class="form-control mb-2" rows="2" value="${this.cardiovascularDiseases}"></textarea>
                </div>       
            </div>
        </div>
        <div id="dentist-fields" class="row mb-2 disappear">
            <label for="#last-visit-date" class="col-sm-4 col-form-label">Дата останнього відвідування:</label>
            <div class="col-sm-8">
                <input type="date" id="last-visit-date" class="form-control mb-2" name="dateOfLastVisit" value="${this.lastVisitDate}">
            </div>
        </div>
        <div id="therapist-fields" class="row mb-2 disappear">
            <label for="#age" class="col-sm-1 col-form-label">Вік:</label>
            <div class="col-sm-4">
                <input type="number" id="age" class="form-control mb-2" name="age" min="1" max="130" value="${this.age}">
            </div>
        </div>

        <button id="btn-create-card" class="btn btn-sm disappear">Створити</button>
    </form>
    `;
    }

    renderFormVisit() {
        super.render();

        const form = document.querySelector('.doctorVisitForm');
        const selectDoctor = document.querySelector('#select-doctor');
        const allDoctorsFields = document.querySelector('#all-doctors-fields');
        const cardiologistFields = document.querySelector('#cardiologist-fields');
        const dentistFields = document.querySelector('#dentist-fields');
        const therapistFields = document.querySelector('#therapist-fields');
        const btnCreateCard = document.querySelector('#btn-create-card');


        form.addEventListener('change', (e) => {
            
            if(e.target === selectDoctor) {

                if(e.target.value === 'cardiologist') {
                    allDoctorsFields.classList.remove('disappear');
                    cardiologistFields.classList.remove('disappear');
                    dentistFields.classList.add('disappear');
                    therapistFields.classList.add('disappear');
                    btnCreateCard.classList.remove('disappear');

                } else if(e.target.value === "dentist") {
                    allDoctorsFields.classList.remove('disappear');
                    dentistFields.classList.remove('disappear');
                    cardiologistFields.classList.add('disappear');
                    therapistFields.classList.add('disappear');
                    btnCreateCard.classList.remove('disappear');
                    
                } else if(e.target.value === "therapist") {
                    allDoctorsFields.classList.remove('disappear');
                    therapistFields.classList.remove('disappear');
                    cardiologistFields.classList.add('disappear');
                    dentistFields.classList.add('disappear');
                    btnCreateCard.classList.remove('disappear');
                }
            }    
        });
    }
}



export const cardsContainer = document.querySelector('#root');
export const noCardsText = document.querySelector('.no-cards');

export class Visit {
    constructor( idCard, fullName, doctor, statusVisit, purposeVisit, description, urgency ) {
        this.statusVisit = statusVisit;
        this.idCard = idCard;
        this.fullName = fullName;
        this.doctor = doctor;
        this.purposeVisit = purposeVisit;
        this.description = description;
        this.urgency = urgency;
        this.card = document.createElement('div');
    }
    renderCardGeneral(cardsContainer) {
        this.card.insertAdjacentHTML('beforeend', ` 
        <div>
            <button type="button" id="btn-edit" class="btn btn-default btn-sm btn-small" data-hide="true" tabindex="-1"><i class="fa fa-edit"></i></button>
            <button type="button" id="btn-close" class="btn btn-default btn-sm btn-small" data-hide="true" tabindex="-1"><i class="bi bi-x-square"></button>
        </div>
        <div class="card-body">
            <h4 class="card-title">Пацієнт: ${this.fullName}</h4>
            <h5 class="card-subtitle">Доктор: ${this.doctor}</h5> 
            <h5 class="card-subtitle"><span class="card-status">Статус візиту: ${this.statusVisit}</span></h5> 
            <div class="accordion">
                <button id="show-more" type="button" data-target="#link-element-${this.idCard}">
                    Show more
                </button>
                <div id="link-element-${this.idCard}">
                    <ul class="card-list">
                        <li class="card-list-item">Терміновість: ${this.urgency}</li>
                        <li class="card-list-item">Мета візиту: ${this.purposeVisit}</li>
                        <li class="card-list-item">Опис візиту: ${this.description}</li>
                    </ul>
                </div>
             </div>
        </div>
        `);
        this.cardList = this.card.querySelector('.card-list')
        cardsContainer.append(this.card);
    }
}

export class VisitDentist extends Visit {
    constructor( idCard, fullName, doctor, statusVisit, purposeVisit, description, urgency, lastVisitDate ) {
        super( idCard, fullName, doctor, statusVisit, purposeVisit, description, urgency );
        this.lastVisitDate = lastVisitDate;
    }
    renderVisitDentist(cardsContainer) {
        super.renderCardGeneral(cardsContainer);
        
        this.card.classList.add('dentist')
        this.card.querySelector('.accordion').classList.add('dentist')
        this.cardList.insertAdjacentHTML("beforeend", `<li class="card-list-item">Дата останнього візиту: ${this.lastVisitDate}</li>`)
        cardsContainer.append(this.card);
    }
}






