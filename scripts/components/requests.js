const tokenAPI = 'https://ajax.test-danit.com/api/v2/cards/login';
const API = 'https://ajax.test-danit.com/api/v2/cards';

export async function getToken(email, password) {
    const response = await fetch(tokenAPI, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email,
            password: password,
        }),
    });

    if (response.ok) {
        return response.text();
    } else {
        alert('Користувача з такими даними не зареєстровано!');
    }
}


const sendRequest = async (API, lastPartAPI = '', method = 'GET', options) => {
    return await fetch(`${API}${lastPartAPI}`, {
        method,
        ...options
    })
    .then(response => {
        if (response.ok) {
            if (method !== "DELETE") {
                const result = response.json();
                return result;
            } else {
                return response;
            }
        } else {
            return new Error("Error");
        }
    });
};


export async function getAllCards(API, token) {
    sendRequest(API, ...[,,], {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
}


export async function sendCard(API, token, cardData) {
    sendRequest(API, '', 'POST', {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(cardData)
    });
}


export async function delCard(API, token, cardId) {
    sendRequest(API, `/${cardId}`, 'DELETE', {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
}

export async function getOneCard(API, token, cardId) {
    sendRequest(API, `/${cardId}`, ...[,], {
    headers: {
        'Authorization': `Bearer ${token}`
      }
    });
}

export async function editCard(API, token, cardId, cardData) {
    sendRequest(API, `/${cardId}`, 'PUT', {  
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
        },
    body: JSON.stringify(cardData)
    });
}







