
export const addIdModal = (idModal) => {
    let modalForm = document.querySelector(`${idModal}`);
    modalForm.style.display = 'block';
    document.body.insertAdjacentHTML('beforeend',`<div class="modal-backdrop fade show"></div>`);
};

export function closeIdModal(idModal) {
    let modalForm = document.querySelector(`${idModal}`)
    modalForm.addEventListener('click', (e) => {
    
    if (e.target.classList.contains('modal') || e.target.classList.contains('btn-close')) {
        modalForm.style.display = 'none';

        const modalBackDrop = document.querySelector('.modal-backdrop');
        modalBackDrop.remove();
    }
});
}

export function showLoader() {
    const loader = document.querySelector('.spinner-border');
    loader.classList.remove('disappear');
}

export function hideLoader() {
    const loader = document.querySelector('.spinner-border');
    loader.classList.add('disappear');
}


