import {  ModalSelectDoctor, VisitDentist } from './classes.js';
import { addIdModal, closeIdModal, showLoader, hideLoader } from './functionsModal.js';
import { sendCard } from './requests.js'



export function createVisit() {

    const createVisitBtn = document.querySelector('.btn-create-visit');

    createVisitBtn.addEventListener('click', () => {
        if (!document.querySelector('#select-doctor-modal')) {
            const form = new ModalSelectDoctor();
            form.renderFormVisit();

            closeIdModal('#select-doctor-modal');
                
        } else {
            addIdModal('#select-doctor-modal');
        }
    });
};


export let allCards = [];


export function createCardVisit() {

    const btnCreateCardVisit = document.querySelector('#btn-create-card');

    btnCreateCardVisit?.addEventListener('click', async (e) => {
        e.preventDefault();
        const form = document.querySelector('.doctorVisitForm');

        let formData = new FormData(form);
        
        let visitData = formDataToObject(formData);

        visitData.statusVisit = 'Open';

        closeIdModal('#select-doctor-modal');
        showLoader();

        function formDataToObject(formData) {
            let cardObject = {};
            for(let [name, value] of formData) {
                if(value) {
                    cardObject[name] = value;
                }
            }
            return cardObject;
        }
        
        await sendCard(API, token, visitData).then(card => {
            hideLoader();
            allCards.push(card);
            localStorage['allCardsOfVisits'] = JSON.stringify(allCards); 
            
            if (card.doctor === "dentist") {
                const visitCard = new VisitDentist(card);
                visitCard.renderVisitDentist(cardsContainer);
                noCardsText(cardsContainer)
            } 
            // else if (card.doctor === "cardiologist") {
            //     const visitCard = new VisitCardiologist(card);
            //     visitCard.render(cardsContainer);
            //     noCardsText(cardsContainer);
            // } else if (card.doctor === "therapist") {
            //     const visitCard = new VisitTherapist(card);
            //     visitCard.render(cardsContainer);
            //     noCardsText(cardsContainer);
            // }

            function noCardsText(allCards) {
                if(allCards.length === 0) {
                    noCardsText.style.display = 'none';
                }  
            }
            
        });
    });

    
};