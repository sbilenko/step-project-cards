import {  ModalLogin } from './classes.js';
import { getToken } from './requests.js';

export function LOGIN() {
    const btnEnter = document.querySelector('.btn-enter');
    const btnLogout = document.querySelector('.btn-logout');
    const noAuthorization = document.querySelector('.no-authorization')

    function showSomeDetails() {
        const createVisitBtn = document.querySelector('.btn-create-visit');
        const filters = document.querySelector('.wrapper-filtering-form');
        const noCardsText = document.querySelector('.no-cards');
    
        btnEnter.classList.add('disappear');
        noAuthorization.classList.add('disappear')
        btnLogout.classList.remove('disappear');
        createVisitBtn.classList.remove('disappear');            
        filters.classList.remove('disappear');
        noCardsText.classList.remove('disappear');
    }

    if (!sessionStorage.getItem('token')) {

        btnEnter.addEventListener('click', () => {
            if (!document.querySelector('#authorization-form-modal')) {
                const form = new ModalLogin();
                form.render();
    
                const modalForm = document.querySelector('#authorization-form-modal');
                const emailForm = modalForm.querySelector('input[type="email"]');
                const passwordForm = modalForm.querySelector('input[type="password"]');
                const btnForm = modalForm.querySelector('.btn-send');
    
                btnForm.addEventListener('click', (e) => {
                    e.preventDefault();
                    if (emailForm.value && passwordForm.value) {
                        submitForm(modalForm, emailForm.value, passwordForm.value);
                        form.removeInvalidMessage();
                    } else {
                        form.emptyInputMessage();
                    }
                });
    
                modalForm.addEventListener('click', (e) => {
                    
                    if (e.target.classList.contains('modal') || e.target.classList.contains('btn-close')) {
                        modalForm.style.display = 'none';

                        const modalBackDrop = document.querySelector('.modal-backdrop');
                        modalBackDrop.remove();
                        emailForm.value = '';
                        passwordForm.value = '';
                        form.removeInvalidMessage();
                    }
                });


            } else {
                const modalForm = document.querySelector('#authorization-form-modal');
                modalForm.style.display = 'block';
                document.body.insertAdjacentHTML('beforeend',`<div class="modal-backdrop fade show"></div>`);
            }
        });

    
        async function submitForm(form, email, password) {
            const token = await getToken(email, password);
            sessionStorage.token = token;
    
            if (token) {
                showSomeDetails();

                form.style.display = 'none';
    
                const modalBackDrop = document.querySelector('.modal-backdrop');
                modalBackDrop.remove();
            }
        }

    } else {
        showSomeDetails();
    }
    
    btnLogout.addEventListener('click', () => {
        sessionStorage.removeItem('token')
        window.location.reload()
    })

}


